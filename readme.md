# Data
Acquisition data from multiple test run of an instance of a [vortex studio](https://gitlab.com/forestry-operator-digital-twin/simulator) forest exploitation simulation. Every test has it raw data in the form of a JSON, a video of the test run and a labeling JSON files representing the state of the operator..

## raw data
the format of the data is as follow
```
{
    "data": [
        {
            "step": int, //data of the step
            "tree_1": [float;3], // position of the tree
            "tree_2": [float;3],
            "tree_3": [float;3],
            "tree_4": [float;3],
            "tree_5": [float;3],
            "tree_6": [float;3],
            "tree_7": [float;3],
            "tree_8": [float;3],
            "tree_9": [float;3],
            "tree_10": [float;3],
            "position_excavator":[float;3],
            "position_stick": [float;3],
            "position_boom": [float;3],
            "position_hand": [float;3],
            "position_finger_1": [float;3],
            "position_finger_2": [float;3],
            "speed_excavator": [float;3],
            "angular_speed_chassis": float,
            "angular_pos_chassis": float
        }
        ...
    ],
    "metadata": {
        "time_interval": int,
        "unit_of_time": string,
        "unit_of_distance": string,
        "unit_of_angle": string,
        "starting_time_of_recording_unix": float
    }
}
```

## Labeling
the format of the labeling is as below
```
{
    "data":[
        {
            "to": int, // step the behavior starts
            "from": int, // step the behavior and
            "state": State // as defined below
        }
        ...
    ],
    "metadata": {
        "number_of_interval": int,
        "date_of_labeling": float
    } 
}
```

### State definition
The `State`(s) are defined below

```
0 : transport a tree to the pile
1 : rotate the excavator to transport the tree into the pile
2 : drop the tree into the pile
3 : adjust the position of the arm to drop the tree
4 : return the arm into position to grab another tree

5 : horizontal rotation of the cabin to grab a tree
6 : vertical rotation of the arm to grab a tree
7 : grab a tree

8 : move toward a tree
9 : move to grab a tree
10 : rotation on oneself
11 : idle

12 : moved hand toward a tree
13 : rotate hand toward a tree
14 : retract tree toward the cabin
15 : move away from a tree for better positioning
16 : adjust the tree for better transportation
17 : retract hand to grab a tree
```
